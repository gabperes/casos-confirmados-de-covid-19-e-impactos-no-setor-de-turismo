
# Projeto:Casos confirmados de COVID-19 e impactos no setor de Turismo

## Descrição:
Este trabalho busca compreender possíveis impactos da COVID-19 no setor do Turismo, nas diversas regiões do Brasil, considerando duas atividades que lhes são diretamente vinculadas.

#OBJETIVOS PRINCIPAIS: 

1. Existe alguma associação entre o crescimento do número de casos de COVID-19 e as alterações
no número de viagens aéreas nacionais/internacionais tendo por origem/destino as diversas
regiões do Brasil (Norte, Sul, Nordeste, Centro-Oeste, Sudeste), ao longo do ano de 2020?
2. Considerando as diversas regiões do Brasil (Norte, Sul, Nordeste, Centro-Oeste, Sudeste), existe
correlação entre o crescimento de casos confirmados de COVID-19 e o número de admissões e de
demissões no setor de turismo, ao longo do ano de 2020?
3. Qual a região do Brasil (Norte, Sul, Nordeste, Centro-Oeste, Sudeste) que teve maior impacto em
relação ao saldo de vagas de emprego formal abertas/fechadas no setor de turismo, ao longo do
ano de 2020?

## Membros:

Gabriel Pena Peres, @gabperes, gab_peres@hotmail.com, Bacharelado em Sistemas de Informação, UTFPR

Cláudia de Oliveira Guimarães de Siqueira, @klauguima, klauguima@gmail.com, Bacharelado em Sistemas de Informação, UTFPR

Bruno Eduardo Procopiuk Walter, @bruno.walter, bruno.walter@ifpr.edu.br, PPGCA-UTFPR


